﻿using UnityEngine;
using System.Collections;

public class SpawnPilot : MonoBehaviour {
	public float begin;
	public float delay;
	public GameObject pilot;
	// Use this for initialization
	void Start () {
		begin = Random.Range (1f, 2f);
		delay = Random.Range (1f, 3f);
		InvokeRepeating("SpawnPilot",begin,delay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void SpawnRandomPilot(){
		//Vector3 position = new Vector3(1, Random.Range(-10.0F, 10.0F), 0);
		//Instantiate(tree, position, Quaternion.identity);
		Instantiate (pilot, transform.position, transform.rotation);
	}
}
