﻿using UnityEngine;
using System.Collections;

public class TreeController : MonoBehaviour {
	public float speed = 5;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * speed * Time.deltaTime);
		if (transform.position.x <= -7f) {
			Destroy(this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "player") {
			Destroy(coll.gameObject);
			this.GameOver();
		}
	}
	public void GameOver()
	{
		Application.LoadLevel("gameover");
	}
}
