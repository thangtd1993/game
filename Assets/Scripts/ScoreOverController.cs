﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreOverController : MonoBehaviour {
	public static int score;
	public Text text;
	public GameObject pilot;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();

	}
	
	// Update is called once per frame
	void Update () {
		text.text = "Score : " + ScoreController.score;
	}
}
