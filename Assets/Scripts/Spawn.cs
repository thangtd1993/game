﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	public float beginTree;
	public float delayTree;
	public float beginPilot;
	public float delayPilot;
	public GameObject tree;
	public GameObject pilot;
	// Use this for initialization
	void Start () {
		beginTree = Random.Range (1f, 2f);
		delayTree = Random.Range (1f, 3f);
		beginPilot = Random.Range (1f, 3.8f);
		delayPilot = Random.Range (2f, 3.5f);
		InvokeRepeating("SpawnTree",beginTree,beginTree);
		InvokeRepeating("SpawnPilot",beginPilot,delayPilot);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void SpawnTree(){
		//Vector3 position = new Vector3(1, Random.Range(-10.0F, 10.0F), 0);
		//Instantiate(tree, position, Quaternion.identity);
		Instantiate (tree, transform.position, transform.rotation);
	}
	void SpawnPilot(){
		Vector3 position = new Vector3(7, Random.Range(-2F, 9F), 0);
		Instantiate(pilot, position, Quaternion.identity);
		Instantiate (pilot, transform.position, transform.rotation);
	}
}
