﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float down = 5;
	public float fly = 3;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey ("space") && (transform.position.y <= 4.8f)) {
			transform.Translate (Vector3.up * fly * Time.deltaTime);
		} else
		{
			transform.Translate (Vector3.down * down *Time.deltaTime);
			if (transform.position.y <= -5f)
			{
				Destroy(this.gameObject);
				this.GameOver();
			}
		}
	}
	void GameOver()
	{

		Application.LoadLevel("gameover");
	}
}
