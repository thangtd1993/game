﻿using UnityEngine;

using System.Collections;

public class PilotController : MonoBehaviour {
	public float speed = 8;


	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * speed * Time.deltaTime);
		if (transform.position.x <= -7f) {
			Destroy(this.gameObject);
			ScoreController.score += 1;
			ScoreOverController.score += 1;

		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "player") {
			Destroy(coll.gameObject);
			this.GameOver();
		}
	}
	void GameOver()
	{

		Application.LoadLevel("gameover");
	}
}
